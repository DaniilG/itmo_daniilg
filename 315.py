n = int(input())
answer = 0

for i in range(1, n + 1):
    result = i ** 2
    answer += result

print(answer)
