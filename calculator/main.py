from calc import operations, history


# Ввод чисел
def user_input(*input_num):
    input_num = input()
    if input_num == 'log':
        history.read_log()
        print("Введите первое число или 'log'")
        user_input()
    elif input_num.replace('.', '', 1).isdigit() == False:
        print("+" * 20, "\nНужно число, а не эта фигня!\n", "+" * 20)
        print("\nПРОБУЙ ЕЩЕ РАЗ ! :\n")
        user_input()
    else:
        num_db.append(input_num)


# MAINLOOP
while True:

    num_db = []
    # Ввод чисел и символа операции
    print("\nВведите первое число или 'log' чтобы посмотреть историю операций:")
    user_input()

    print("\nКакое действие будем выполнять? Напиши ' + , - , * , / , ** ': ")
    # Проверка символа операции
    sym_list = ('+', '-', '*', '/', '**')
    for symbol in sym_list:
        symbol = input()
        if symbol not in sym_list:
            print('\nНеверно введен символ!! Введи еще раз: \n')
            continue
        else:
            loging_sym = symbol
            break

    print("\nвведите ВТОРОЕ число  \n")
    user_input()


    # Вывод результата
    result = operations.operations(symbol, num_db[0], num_db[1])
    print('+' * 20)
    print('Получается: ', result)
    print('+' * 20)


    # Запись данных
    history.loging(history.time_str, num_db[0], loging_sym, num_db[1], result)


    # Закрытие программы
    print('\n' * 2, "Закончил? Y / N :  ")
    final = input().lower()
    if final[0] == 'n':
        print('\n', '#' * 20, 'ОКЕЙ, ПРОДОЛЖАЕМ', '#' * 20, '\n')
    else:
        print('\n' * 3, '=' * 20, 'СПАСИБО ЧТО БЫЛИ С НАМИ', '=' * 20, '\n' * 3)
        break
