import datetime
# import pickle


# Создание и воспроизведение Лога операций
time_data = datetime.datetime.now()
time_str = time_data.strftime('%d/%b/%Y, %H:%M    ==    ')
space = ('\n-------------\n')


def loging(when, num1, sym, num2, equals):
    packed = when + str(num1) + sym + str(num2) + ' = ' + str(equals) + space
    with open ('operations_log.txt', 'a') as logfile:
        logfile.write(packed)
    # Сериализация
    # with open ('operations_log.dat', 'ab') as loginbytes:
    #     pickle.dump(packed, loginbytes)


def read_log():
    with open ('operations_log.txt', 'r') as logfile:
        print(logfile.read())
    # # Десериализация
    # with open ('operations_log.dat', 'rb') as loginbytes:
    #     logged_in_bytes = pickle.load(loginbytes)
    #     print(logged_in_bytes)

