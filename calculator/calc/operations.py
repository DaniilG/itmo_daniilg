# Вычисление
def operations(sym, number_1, number_2):
    if sym == '+':
        return float(number_1) + float(number_2)
    elif sym == '-':
        return float(number_1) - float(number_2)
    elif sym == '*':
        return float(number_1) * float(number_2)
    elif sym == '/':
        return float(number_1) / float(number_2)
    elif sym == '**':
        return float(number_1) ** float(number_2)
        
        