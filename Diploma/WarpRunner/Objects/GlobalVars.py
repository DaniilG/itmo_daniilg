import os
import pygame


class GlobalVar():
    def __init__(self):
        self.game_speed = 60
        self.running = True

        self.player_score = 0

        # Препятствия
        self.obstacles = []

        # Снаряды
        self.bullets = []
        self.shoot_loop = 0
        self.bullet_timer = 0

        # Пауза для падения
        self.pause = 0
        self.fallspeed = 0


# Создание игрового пространства

class GameEnvironment():
    def __init__(self):
        # Окно
        self.win_width = 800
        self.win_height = 447
        self.win = pygame.display.set_mode((self.win_width, self.win_height))

        # Звуки
        self.death_sound = pygame.mixer.Sound(os.path.join('Sounds', 'gameover.wav'))
        self.dead_sound = pygame.mixer.Sound(os.path.join('Sounds', 'dead.wav'))
        self.jump_sound = pygame.mixer.Sound(os.path.join('Sounds', 'jump.wav'))
        self.bullet_sound = pygame.mixer.Sound(os.path.join('Sounds', 'bullet.wav'))
        self.music = pygame.mixer.music.load(os.path.join('Sounds', 'HVOB.mp3'))

        # Изображение
        self.bg_img = pygame.image.load(os.path.join('Images', 'bg.jpeg'))
        self.bg = pygame.transform.scale(self.bg_img, (800, 447))
        self.bgx = 0#
        self.bgx2 = self.bg.get_width()
        self.rip = pygame.image.load(os.path.join('Images', 'riped.jpg'))
        self.rip_img = pygame.transform.scale(self.rip, (800, 447))
        self.bullet_img = pygame.image.load(os.path.join('Images', 'bullet.png'))
