import os
import pygame


class Saw():
    img = [
        pygame.image.load(os.path.join('Images', 'SAW0.png')),
        pygame.image.load(os.path.join('Images', 'SAW1.png')),
        pygame.image.load(os.path.join('Images', 'SAW2.png')),
        pygame.image.load(os.path.join('Images', 'SAW3.png')),
    ]


    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.count = 0
        self.vel = 1.4
        self.visible = True
        self.health = 5
        self.hitbox = (self.x + 15, self.y + 20, self.width - 30, self.height - 15)


    def draw(self, win):
        if self.visible:
            self.hitbox = (self.x + 15, self.y + 20, self.width - 30, self.height - 15)
            # Показать HitBox
            # pygame.draw.rect(win, (255, 0, 0), self.hitbox, 2)

            if self.count >= 8:
                self.count = 0

            win.blit(pygame.transform.scale(self.img[self.count // 2], (100, 100)), (self.x, self.y))
            self.count += 1


    def hit(self, rect):
        if self.visible:
            if rect[0] + rect[2] > self.hitbox[0] and rect[0] < self.hitbox[0] + self.hitbox[2]:
                if rect[1] + rect[3] > self.hitbox[1]:
                    return True
        else:
            return False


    def get_shot(self, bullet):
        if bullet.x + bullet.radius > self.hitbox[0] and bullet.x < self.hitbox[0] + self.hitbox[2]:
            if bullet.y > self.hitbox[1]:
                return True

            return False


class Spike(Saw):
    img = pygame.image.load(os.path.join('images', 'spike1.png'))


    def draw(self, win):
        if self.visible:
            self.hitbox = (self.x + 20, self.y, 20, 315)
            # Показать HitBox
            # pygame.draw.rect(win, (255, 0, 0), self.hitbox, 2)
            if self.visible:
                win.blit(self.img, (self.x, self.y))


    def hit(self, rect):
        if self.visible:
            if rect[0] + rect[2] > self.hitbox[0] and rect[0] < self.hitbox[0] +self.hitbox[2]:
                if rect[1] < self.hitbox[3]:
                    return True
        else:
            return False


    def get_shot(self, bullet):
        if bullet.x + bullet.radius > self.hitbox[0] and bullet.x < self.hitbox[0] + self.hitbox[2]:
            if bullet.y < self.hitbox[1] + self.hitbox[3]:
                return True

            return False
