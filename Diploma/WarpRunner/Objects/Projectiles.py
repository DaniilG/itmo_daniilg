import pygame


class Projectile():  # Снаряды
    def __init__(self, x, y, radius, color, facing):
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color
        self.vel = 8


    def draw(self, win):
        pygame.draw.circle(win, self.color, (self.x, self.y), self.radius)
