import os
import pygame


class Player():
    run = [pygame.image.load(os.path.join('images', str(x) + '.png')) for x in range(8, 16)]
    jump = [pygame.image.load(os.path.join('images', str(x) + '.png')) for x in range(1, 8)]
    fall = pygame.image.load(os.path.join('Images', '0.png'))

    slide = [
        pygame.image.load(os.path.join('images', 'S1.png')),
        pygame.image.load(os.path.join('images', 'S2.png')),
        pygame.image.load(os.path.join('images', 'S2.png')),
        pygame.image.load(os.path.join('images', 'S2.png')),
        pygame.image.load(os.path.join('images', 'S2.png')),
        pygame.image.load(os.path.join('images', 'S2.png')),
        pygame.image.load(os.path.join('images', 'S2.png')),
        pygame.image.load(os.path.join('images', 'S2.png')),
        pygame.image.load(os.path.join('images', 'S3.png')),
        pygame.image.load(os.path.join('images', 'S4.png')),
        pygame.image.load(os.path.join('images', 'S5.png')),
    ]

    jump_list = [
        1, 1, 1, 1, 1, 1,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        -1, -1, -1, -1, -1, -1,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
        -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,
    ]


    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.jumping = False
        self.sliding = False
        self.slide_count = 0
        self.jump_count = 0
        self.run_count = 0
        self.slide_up = False
        self.vel = 3
        self.falling = False


    def draw(self, win):
        if self.falling:
            win.blit(self.fall, (self.x, self.y + 30))
        elif self.jumping:

            # if self.jump_count >= -10:
            #     self.y -= (self.jump_count * abs(self.jump_count)) / 2
            #     print(self.jump_count)
            #     self.jump_count -= 1
            # else:
            #     self.jump_count = 10
            #     self.jumping = False

            self.y -= self.jump_list[self.jump_count] * 1.2
            win.blit(self.jump[self.jump_count // 18], (self.x, self.y))
            self.jump_count += 1
            if self.jump_count > 108:
                self.jump_count = 0
                self.jumping = False
                self.run_count = 0

            self.hitbox = (self.x + 4, self.y, self.width - 24, self.height - 10)

        elif self.sliding or self.slide_up:
            if self.slide_count < 20:
                self.y += 1
                self.hitbox = (self.x + 4, self.y, self.width - 24, self.height - 10)

            elif self.slide_count == 80:
                self.y -= 19
                self.sliding = False
                self.slide_up = True
            elif self.slide_count > 20 and self.slide_count < 80:
                self.hitbox = (self.x, self.y + 3, self.width - 8, self.height - 35)

            if self.slide_count >= 110:
                self.slide_count = 0
                self.slide_up = False
                self.run_count = 0
                self.hitbox = (self.x + 4, self.y, self.width - 24, self.height - 10)

            win.blit(self.slide[self.slide_count // 10], (self.x, self.y))
            self.slide_count += 1

        else:
            if self.run_count > 42:
                self.run_count = 0

            win.blit(self.run[self.run_count // 6], (self.x, self.y))
            self.run_count += 1
            self.hitbox = (self.x + 4, self.y, self.width - 24, self.height - 13)

        # Показать HitBox
        # pygame.draw.rect(win, (255,0,0),self.hitbox, 2)
