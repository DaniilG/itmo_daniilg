import os
import pygame
from pygame.locals import *
from random import randrange
from Objects.Player import Player
from Objects.Projectiles import Projectile
from Objects.Saw_Spike import Saw, Spike
from Objects.GlobalVars import GlobalVar, GameEnvironment


# Функция отрисовки объектов
def redrawwindow():
    game_env.win.blit(game_env.bg, (game_env.bgx, 0))
    game_env.win.blit(game_env.bg, (game_env.bgx2, 0))
    man.draw(game_env.win)

    for item in gl_var.obstacles:  # Отрисовка препятствий
        item.draw(game_env.win)

    for bullet in gl_var.bullets:  # Отрисовка снарядов
        bullet.draw(game_env.win)

    score_font = pygame.font.SysFont('castellar', 30)
    score_text = score_font.render(
        'SCORE: ' + str(gl_var.player_score),
        1,
        (250, 223, 46),
    )
    game_env.win.blit(score_text, (630, 15))

    keys_font = pygame.font.SysFont('chiller', 18)
    keys_text = keys_font.render(
        'KEYS: UP,  DOWN, LEFT, RIGHT, SPACE',
        1,
        (191, 191, 178),
    )
    game_env.win.blit(keys_text, (10, 10))

    if gl_var.bullet_timer == 1:
        game_env.win.blit(game_env.bullet_img, (30, 40))

    pygame.display.update()


# Фон перезагружается каждый раз когда предыдущий уходит в отрицательный x
def bg_change():
    game_env.bgx -= 1
    game_env.bgx2 -= 1

    if game_env.bgx < game_env.bg.get_width() * -1:
        game_env.bgx = game_env.bg.get_width()
    if game_env.bgx2 < game_env.bg.get_width() * -1:
        game_env.bgx2 = game_env.bg.get_width()


# Показ финального экрана
def deadly_falling():
    if gl_var.pause > 0:
        gl_var.pause += 1
        if gl_var.pause > gl_var.fallspeed * 2:
            the_end_screen()


# Финальный экран и перезапуск игры
def the_end_screen():
    pygame.mixer.music.stop()
    # Требуется обнулить глобальные переменные
    gl_var.pause = 0
    gl_var.obstacles = []
    gl_var.game_speed = 45

    game_env.death_sound.play()
    running = True
    while running:
        pygame.time.delay(100)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                running = False

        game_env.win.blit(game_env.rip_img, (0, 0))
        end_font = pygame.font.SysFont('chiller', 80)
        prev_score = end_font.render('Best result: ' + str(score_r_w()), 1, (201, 10, 10))
        game_env.win.blit(prev_score, (game_env.win_width // 2 - prev_score.get_width() // 2, 100))
        your_score = end_font.render('Your score: ' + str(gl_var.player_score), 1, (201, 10, 10))
        game_env.win.blit(your_score, (game_env.win_width // 2 - your_score.get_width() // 2, 200))
        pygame.display.update()

    gl_var.player_score = 0
    man.falling = False
    man.x = 200
    pygame.mixer.music.play()


# Запись и обновление лучшего результата
def score_r_w():
    f = open('scores.txt', 'r')
    file = f.readlines()
    best_score = int(file[0])
    if best_score < int(gl_var.player_score):
        f.close()
        with open('scores.txt', 'w') as score_file:
            score_file.write(str(gl_var.player_score))

        return gl_var.player_score
    return best_score


# Столкновение с препятствием
def hit_obstacle():
    for item in gl_var.obstacles:
        if item.hit(man.hitbox):
            game_env.dead_sound.play()
            man.falling = True

            if gl_var.pause == 0:
                gl_var.fallspeed = gl_var.game_speed
                gl_var.pause = 1
        # Удаление препятствий ушедших за экран
        item.x -= 1
        if item.x < item.width * -1:
            gl_var.obstacles.pop(gl_var.obstacles.index(item))


# Стрельба
def shooting():
    # gl_var.shoot_loop = 0
    # if gl_var.shoot_loop > 0:
    #     gl_var.shoot_loop += 1
    # if gl_var.shoot_loop > 3:
    #     gl_var.shoot_loop = 0

    for bullet in gl_var.bullets:
        if bullet.x < 805 and bullet.x > 0:
            bullet.x += bullet.vel
        else:
            gl_var.bullets.pop(gl_var.bullets.index(bullet))

    keys = pygame.key.get_pressed()
    if keys[pygame.K_SPACE]:

        # if len(gl_var.bullets) < 1:
        if gl_var.bullet_timer == 1 and not man.falling:
            game_env.bullet_sound.play()
            gl_var.bullets.append(Projectile(round(
                man.x + man.width // 2),
                round(man.y + man.height // 2),
                6,
                (31, 194, 11),
                1,
            ))
            gl_var.bullet_timer = 0


# Попадания снарядов в препятствия
def obstacles_get_shot():
    for bullet in gl_var.bullets:
        for obstacle in gl_var.obstacles:
            if obstacle.get_shot(bullet):
                obstacle.visible = False
                gl_var.obstacles.pop(gl_var.obstacles.index(obstacle))
                for bullet in gl_var.bullets:
                    gl_var.bullets.pop(gl_var.bullets.index(bullet))


# Работа с ивентами
def pygame_events():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gl_var.running = False
            quit()

        # Ускорение игры каждые 500 мс
        if event.type == USEREVENT+1:
            gl_var.game_speed += 2

        # Генерация препятствий
        if event.type == USEREVENT+2:
            obstacle_type = randrange(0, 2)
            if obstacle_type == 0:
                gl_var.obstacles.append(Saw(810, 310, 100, 100))
            elif obstacle_type == 1:
                gl_var.obstacles.append(Spike(810, 0, 48, 320))

        # Счетчик очков игрока, 1 секунда = 1 очко
        if event.type == USEREVENT+3:
            gl_var.player_score += 1
            return gl_var.player_score

        if event.type == USEREVENT+4:
            gl_var.bullet_timer = 1


def man_movement():
    keys = pygame.key.get_pressed()

    if keys[pygame.K_UP]:
        game_env.jump_sound.play()
        if not man.jumping:
            man.jumping = True

    if keys[pygame.K_DOWN]:
        if not man.sliding:
            man.sliding = True

    if not man.sliding and not man.falling:
        if keys[pygame.K_LEFT] and man.x > man.vel:
            man.x -= man.vel

        elif keys[pygame.K_RIGHT] and man.x < 800 - man.vel - man.width:
            man.x += man.vel

pygame.init()


# глобальные объекты

gl_var = GlobalVar()
game_env = GameEnvironment()
man = Player(200, 313, 64, 64)
clock = pygame.time.Clock()

pygame.display.set_caption('Warp Runner')

bg_music = game_env.music
pygame.mixer.music.play(-1)

# Ускорение игры каждые 500 мс
pygame.time.set_timer(USEREVENT+1, 500)

# Рандомное появление препятствий
pygame.time.set_timer(USEREVENT+2, randrange(3000, 5000))

# Таймер очков игрока, 1 секунда = 1 очко
pygame.time.set_timer(USEREVENT+3, 1000)

# Таймер появления снарядов
pygame.time.set_timer(USEREVENT+4, 5000)


# MAINLOOP

while gl_var.running:

    # Проверка финального экрана
    deadly_falling()

    # Столкновение с препятствием и удаление ушедших за экран
    hit_obstacle()

    # Фон перезагружается каждый раз когда предыдущий уходит в отрицательный x
    bg_change()

    # Закрытие программы, ускорение геймплея, генерация препятствий, счетчик очков
    pygame_events()

    # Снаряды
    shooting()
    obstacles_get_shot()

    # Движения игрока
    man_movement()


    clock.tick(gl_var.game_speed)
    redrawwindow()
