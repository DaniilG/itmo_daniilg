# Сегодняшнее число
todaydate = input('Введите сегодняшнюю дату в формате Д.М.ГГГГ: ').split('.')
todaydate_d, todaydate_m, todaydate_y = todaydate[0], todaydate[1], todaydate[2]

#Дата рождения
birth_day = input('Введите дату рождения в формате Д.М.ГГГГ: ').split('.')
birth_day_d, birth_day_m, birth_day_y = birth_day[0], birth_day[1], birth_day[2]

age = int(todaydate_y) - int(birth_day_y)

# Коррекция возраста относительно даты текущего года (др. уже было или еще нет)
if todaydate_m < birth_day_m or todaydate_m == birth_day_m and todaydate_d < birth_day_d:
    age -= 1
print('Вам {} !'. format(age))

