class Cage:
    def __init__(self, space):
        self.space = space
        self.animals_in_cage = []


    def add(self, animal) -> bool:
        if animal.space < self.space:
            self.animals_in_cage.append(animal.name)
            self.space -= animal.space
            return True
        else:
            return False


    def free_space(self) -> int:
        return self.space


    def get_animals(self) -> list:
        return self.animals_in_cage


class Animal:
    def __init__(self, name, space):
        self.name = name
        self.space = space



cage = Cage(200)
lion = Animal('Alex', 100)
pinguin1 = Animal('Gunter', 15)
pinguin2 = Animal("Ganter", 25)
begemoth = Animal("Gloria", 300)
giraff = Animal("Melvin", 70)


print(cage.add(pinguin1))          #True
print(cage.add(begemoth))          #False
print(cage.free_space())           #185
print(cage.add(lion))              #True
print(cage.free_space())           #85
print(cage.add(pinguin2))          #True
print(cage.add(giraff))            #False
print(cage.get_animals())          #['Gunter', 'Alex', 'Ganter']
