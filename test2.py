# 6. Поставка специализированной техники. Контрагенты (клиенты /
# поставщики / субподрядчики на сервисе / кредитные организации).

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from datetime import datetime



Base = declarative_base()


# СПИСОК ИМЕЮЩЕЙСЯ ТЕХНИКИ
class Product(Base):
    __tablename__ = 'products'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(80), nullable=False, unique=True)
    price = sa.Column(sa.Float, nullable=False)


# ПРОДАЖИ
class Sale(Base):
    __tablename__ = 'sales'

    id = sa.Column(sa.Integer, primary_key=True)
    product_name = sa.Column(
        sa.String, sa.ForeignKey(f'{Product.__tablename__}.name'),
    )
    amount = sa.Column(sa.Float, nullable=False)
    whosold =  sa.Column(sa.String(64), nullable=False)
    date = sa.Column(sa.Date, nullable=False)


# МАНАГЕРЫ
class SalesManagers(Base):
    __tablename__ = 'salesmanagers'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64), nullable=False, unique=True)
    salary = sa.Column(sa.Float, nullable=False)


# РОЛИ В КОМПАНИИ
class Role:
    Session = sessionmaker()

    def __init__(self, url):
        self.engine = sa.create_engine(url)
        self.session = self.Session(bind=self.engine)
        Base.metadata.create_all(self.engine)


# ПРОДАЖНИК
class SaleManager(Role):
    def sale(self, name, whosold, amount):
        product = self.session.query(Product).filter(
            Product.name == name
        ).first()
        sale = Sale(
            product_name=product.name,
            whosold=whosold,
            amount=amount,
            date=datetime.now()
        )
        self.session.add(sale)
        self.session.commit()


# ДИРЕКТОР И ЧТО ОН МОЖЕТ
class HeadRole(Role):
# менять цены
    def change_price(self, name, price):
        product = self.session.query(Product).filter(
            Product.name == name
        ).first()
        product.name=name
        product.price=price
        self.session.commit()

# добавлять продукцию
    def add_product(self, name, price):
        self.session.add(
            Product(name=name, price=price),
        )
        self.session.commit()

# нанимать продажников
    def add_manager(self, name, salary):
        self.session.add(
            SalesManagers(name=name, salary=salary)
        )
        self.session.commit()


director = HeadRole(url='sqlite:///db2.sqlite3')
salesmanager = SaleManager(url='sqlite:///db2.sqlite3')

# ДОБАВЛЯЕМ ТОВАРЫ
director.add_product(name='Шиберная задвижка', price=122000)
director.add_product(name='Вертлюг', price=2654.55)
director.add_product(name='Отвертка', price=18.50)
director.add_product(name='Холодильник кипящего слоя', price=540000)

# НАНИМАЕМ МЕНЕДЖЕРОВ
director.add_manager(name='Кейси Райбек', salary=40000)
director.add_manager(name='Джон Рэмбо', salary=55000)
director.add_manager(name='Николаева Елена', salary=70000.50)

# ПРОДАЕМ ТОВАРЫ
salesmanager.sale(name='Шиберная задвижка', whosold="Джон Рэмбо", amount=3)
salesmanager.sale(name='Отвертка', whosold="Кейси Райбек", amount=50)
salesmanager.sale(name='Холодильник кипящего слоя', whosold='Николаева Елена', amount=10)

# МЕНЯЕМ ЦЕНЫ
director.change_price(name='Вертлюг', price=666)
