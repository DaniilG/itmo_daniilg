string1_set = set(input())
string2_set = set(input())
string3_set = set(input())

print('\n в первой строке {} уникальных символов'.format(len(string1_set)))
print('\n во второй строке {} уникальных символов'.format(len(string2_set)))
print('\n в третьей строке {} уникальных символов'.format(len(string3_set)))

#колво всех уникальных во всех строках
quantity = string1_set | string2_set | string3_set

#колво общих уникальных символов
same_sym = string1_set & string2_set & string3_set

print('\n количество общих уникальных символов:', len(same_sym))
print('\n количество всех уникальных символов всех трех строк: ', len(quantity))

