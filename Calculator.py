# Ввод чисел
def user_input(*input_num):
    input_num = input()
    if input_num.replace('.', '').isdigit() == False:
        print("+" * 20, "\nНужно число, а не эта фигня!\n", "+" * 20)
        print("\nПРОБУЙ ЕЩЕ РАЗ ! :\n")
        user_input()
    else:
        num_db.append(input_num)


# Вычисление
def operations(sym, number_1, number_2):
    if sym == '+':
        return float(number_1) + float(number_2)
    elif sym == '-':
        return float(number_1) - float(number_2)
    elif sym == '*':
        return float(number_1) * float(number_2)
    elif sym == '/':
        return float(number_1) / float(number_2)
    elif sym == '**':
        return float(number_1) ** float(number_2)


# MAINLOOP
while True:

    num_db = []
    # Ввод чисел и символа операции
    print("\nВведите первое число:  ")
    user_input()

    print("\nКакое действие будем выполнять? Напиши ' + , - , * , / , ** ': ")
    # Проверка символа операции
    sym_list = ('+', '-', '*', '/', '**')
    for symbol in sym_list:
        symbol = input()
        if symbol not in sym_list:
            print('\nНеверно введен символ!! Введи еще раз: \n')
            continue
        else:
            break

    print("\nвведите ВТОРОЕ число  \n")
    user_input()


    # Вывод результата
    print('+' * 20, '\n', 'РЕЗУЛЬТАТ СЧЕТА {}!'.format(operations(symbol,
                                                                  num_db[0],
                                                                  num_db[1])),
          '\n', '+' * 20)


    # Закрытие программы
    print('\n' * 2, "Закончил? Y / N :  ")
    final = input().lower()
    if final[0] == 'n':
        print('\n', '#' * 20, 'ОКЕЙ, ПРОДОЛЖАЕМ', '#' * 20, '\n')
    else:
        print('\n' * 3, '=' * 20, 'СПАСИБО ЧТО БЫЛИ С НАМИ', '=' * 20, '\n' * 3)
        break

