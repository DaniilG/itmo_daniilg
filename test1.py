# Вывести на экран числа от 1000 до 9999 такие, что все цифры различны
start = 1000
# Перебор чисел
while start <= 9999:
    if len(set(str(start))) == 4:
        print(start)

    start += 1

