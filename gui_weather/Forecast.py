import sys
import requests
from PyQt5 import QtWidgets, QtGui
# from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter
from weathGui import Ui_MainWindow


class WeatherWin(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.ButtonOk.clicked.connect(self.button_clicked)
        self.qp = QPainter()
        self.errormes = 'Неверный ввод или ошибка сервера, повторите попытку позже'


    def button_clicked(self):
        self.city = self.ui.cityinput.text()
        self.getweather(self.city)

        try:
            self.ui.tempoutput.setText(self.temp)
            self.wind_arrow(self.wind)
        except AttributeError:
            self.ui.errormessage.setText(self.errormes)


    def getweather(self, city):
        appid = 'e32e7c65078c472a6d038a9d83d9f665'
        response = requests.get(
            'http://api.openweathermap.org/data/2.5/weather',
            params={
                "q":  city,
                "appid": appid,
            },
        )
        weath_data = response.json()

        try:
            # Погода всегда указывается целым числом, по этому округляем
            self.temp = str(int(weath_data['main']['temp'] - 273))

            # Для случаев когда ветра нет и json приходит без ключа ['wind']['deg']
            try:
                self.wind = weath_data['wind']['deg']
            except KeyError:
                return self.temp

        except KeyError:
            self.ui.errormessage.setText(self.errormes)
        else:
            self.ui.errormessage.clear()
            return self.temp, self.wind


    def wind_arrow(self, wind_deg):
        image_fname = ('windarrow.png')
        matrix = QtGui.QTransform()
        matrix.rotate(wind_deg)
        pix = QtGui.QPixmap(image_fname).transformed(QtGui.QTransform(matrix))
        self.ui.windoutput.setPixmap(pix)


# Запуск
app = QtWidgets.QApplication([])
win = WeatherWin()
win.show()

sys.exit(app.exec())

