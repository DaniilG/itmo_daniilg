# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'weathGui2.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(467, 366)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.ButtonOk = QtWidgets.QPushButton(self.centralwidget)
        self.ButtonOk.setGeometry(QtCore.QRect(140, 140, 191, 61))
        font = QtGui.QFont()
        font.setPointSize(22)
        font.setBold(True)
        font.setWeight(75)
        self.ButtonOk.setFont(font)
        self.ButtonOk.setObjectName("ButtonOk")
        self.headlabel = QtWidgets.QLabel(self.centralwidget)
        self.headlabel.setGeometry(QtCore.QRect(30, 10, 411, 31))
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.headlabel.setFont(font)
        self.headlabel.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.headlabel.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.headlabel.setObjectName("headlabel")

        self.cityinput = QtWidgets.QLineEdit(self.centralwidget)
        self.cityinput.setGeometry(QtCore.QRect(70, 60, 341, 41))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.cityinput.setFont(font)
        self.cityinput.setMaxLength(80)
        self.cityinput.setFrame(True)
        self.cityinput.setObjectName("cityinput")

        self.errormessage = QtWidgets.QLabel(self.centralwidget)
        self.errormessage.setGeometry(QtCore.QRect(15, 111, 460, 19))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.errormessage.setFont(font)

        self.tempoutput = QtWidgets.QLineEdit(self.centralwidget)
        self.tempoutput.setGeometry(QtCore.QRect(140, 260, 51, 41))
        font = QtGui.QFont()
        font.setPointSize(22)
        font.setBold(True)
        font.setWeight(75)
        self.tempoutput.setFont(font)
        self.tempoutput.setObjectName("tempoutput")

        self.windoutput = QtWidgets.QLabel(self.centralwidget)
        self.windoutput.setGeometry(QtCore.QRect(340, 260, 51, 41))
        self.windoutput.setObjectName("windoutput")

        self.templine = QtWidgets.QLabel(self.centralwidget)
        self.templine.setGeometry(QtCore.QRect(10, 270, 121, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(False)
        font.setWeight(50)
        self.templine.setFont(font)
        self.templine.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.templine.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.templine.setLineWidth(12)
        self.templine.setIndent(2)
        self.templine.setObjectName("templine")
        self.windline = QtWidgets.QLabel(self.centralwidget)
        self.windline.setGeometry(QtCore.QRect(200, 250, 131, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(False)
        font.setWeight(50)
        self.windline.setFont(font)
        self.windline.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.windline.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.windline.setLineWidth(12)
        self.windline.setIndent(2)
        self.windline.setObjectName("windline")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.ButtonOk.setText(_translate("MainWindow", "УЗНАТЬ!"))
        self.headlabel.setText(_translate("MainWindow", "Введите город чтобы узнать погоду"))
        self.templine.setText(_translate("MainWindow", "Температура"))
        self.windline.setText(_translate("MainWindow", "Направление\n"
"ветра"))

