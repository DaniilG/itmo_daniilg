# Раз три варианта решения, то должны работать все три варианта
# Первый вариант не работает. Попробуйте тест: 1 1 2 3 4 3
# Последний вариант - из пушки по воробьям, сортировка более требовательна к ресурсам
# Исправил первый вариант

nums = input('Введите 5 чисел разделяя их запятой: ').split(',')
first = int(nums[0])
second = int(nums[1])
third = int(nums[2])
fourth = int(nums[3])
fifth = int(nums[4])

# Вариант решения №1 - поиск наименьшего числа с помощью Операторов сравнения
if first <= second and first <= third and first <= fourth and first <= fifth:
    print('Наименьшее число: ', first)
elif second <= first and second <= third and second <= fourth and second <= fifth:
    print('Наименьшее число: ', second)
elif third <= first and third <= second and third <= fourth and third <= fifth:
    print('Наименьшее число: ', third)
elif fourth <= second and fourth <= third and fourth <= first and fourth <= fifth:
    print('Наименьшее число: ', fourth)
else:
    print('Наименьшее число: ', fifth)

# Вариант решения №2 - создаем список из пяти строк
sorting_list = []
sorting_list.append(first)
sorting_list.append(second)
sorting_list.append(third)
sorting_list.append(fourth)
sorting_list.append(fifth)

# выводим наименьшее число с помощью фунции списков "min()"
print('Наименьшее число: ', min(sorting_list))

# Вариант решения №3 - выводим наименьшее число с помощью фунции сортировки списка
sorting_list.sort()
print('Наименьшее число: ', sorting_list[0])

