# Логику ввода и вывода лучше разделять, например, пустой строкой
# Принято

num = int(input())
print('The next number for the number {} is {}.'.format(num, num+1))
print('The previous number for the number {} is {}.'.format(num, num-1))
